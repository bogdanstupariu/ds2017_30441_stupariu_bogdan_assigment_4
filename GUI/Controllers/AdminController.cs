﻿using GUI.AdminService;
using GUI.UserService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace GUI.Controllers
{
    public class AdminController : Controller
    {

        public UserService.User[] allUsers;
        public AdminService.Package[] allPackages;
        public AdminService.Tracking[] packageTrack;
        private UserServiceSoapClient _userServiceSOAP = new UserServiceSoapClient();
        private AdminServiceSoapClient _adminServiceSOAP = new AdminServiceSoapClient();

        public AdminController()
        {
            ViewBag.SuccessMsg = "";
            ViewBag.ErrorMsg= "";
            ViewBag.packageListFull = "";
            ViewBag.packageListNotTracked = "";
            ViewBag.packageListTracked = "";
            ViewBag.ErrorMsgTrack = "";
            ViewBag.ErrorMsgTracked = "";
            ViewBag.trackingList = "";
            ViewBag.packageId = "";
            ViewBag.packageName = "";

            allUsers = _userServiceSOAP.GetAllUsers();
            allPackages = _adminServiceSOAP.GetAllPackages();
            ViewBag.packageListFull = allPackages;

            setUserDropdown();
            setPackageDropdown();
           

        }

        [HttpPost]
        public ActionResult AddPackage(string senderCity, string destCity, string name, string description, string sender, string receiver)
        {
            if (senderCity == "" || destCity == "" || name=="" || description == "" || sender == "" || receiver == "")
            {
                ViewBag.ErrorMsg = "All fields should be filled in";
            }

            int sender_id = 0, receiver_id = 0;
            foreach(UserService.User user in allUsers)
            {
                if (user.name == sender) sender_id = user.id;
                if (user.name == receiver) receiver_id = user.id;
            }

            AdminService.Package pack = new AdminService.Package  {
                name = name,
                description = description,
                sender_city = senderCity,
                destination_city = destCity,
                tracking = false,
                sender_id = sender_id,
                receiver_id = receiver_id
            };
            

            _adminServiceSOAP.AddPackage(pack);

            return View("PackagesAdmin");
        }

        [HttpPost]
        public ActionResult RemovePackage(string package)
        {
            if (package == "")
            {
                ViewBag.ErrorMsgRemove = "Select a package to remove";
            } else
            {
                _adminServiceSOAP.RemovePackage(int.Parse(package));
            }
            return View("PackagesAdmin");
        }

        [HttpPost]
        public ActionResult TrackPackage(string package)
        {
            if (package == "")
            {
                ViewBag.ErrorMsgTrack = "Select a package to track";
            }
            else
            {
                _adminServiceSOAP.TrackPackage(int.Parse(package));
            }
            return View("PackagesAdmin");
        }

        [HttpPost]
        public ActionResult SeeTrack(string package)
        {
            if (package == "")
            {
                ViewBag.ErrorMsgTracked = "Select a package to track";
            }
            else
            {
                packageTrack = _adminServiceSOAP.GetPackageTrack(int.Parse(package));

                ViewBag.trackingList = packageTrack;
                ViewBag.packageId = packageTrack[0].Package.id;
                ViewBag.packageName = packageTrack[0].Package.name;
                Session.Add("pack", packageTrack[0].Package.id);

            }
            return View("TrackingAdmin");
        }

        [HttpPost]
        public ActionResult AddTrack(string track)
        {
            string packageTra = Session["pack"].ToString();
            if (track == "" || packageTra == "")
            {
                ViewBag.ErrorMsgAddCity = "Field cannot be empty";
                
            }
            else
            {
                _adminServiceSOAP.AddTrack(track, int.Parse(packageTra));

            }
            Session.Remove("pack");
            return View("TrackingAdmin");
        }

        private void setUserDropdown()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (UserService.User user in allUsers)
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = user.name,
                    Value = user.id.ToString(),
                    Selected = false
                };
                list.Add(selectListItem);
            }

            ViewBag.userListSender = list;
            ViewBag.userListReceiver = list;
        }

        private void setPackageDropdown()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<SelectListItem> notTrackList = new List<SelectListItem>();
            List<SelectListItem> trackedList = new List<SelectListItem>();
            foreach (AdminService.Package package in allPackages)
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = package.name,
                    Value = package.id.ToString(),
                    Selected = false
                };
                if (!package.tracking)
                    notTrackList.Add(selectListItem);
                else
                    trackedList.Add(selectListItem);
                list.Add(selectListItem);
            }

            ViewBag.packageListNotTracked = notTrackList;
            ViewBag.packageList = list;
            ViewBag.packageListTracked = trackedList;
        }

        public ActionResult ViewPackages()
        {
            try { 
                int sessionRole = int.Parse(Session["role"].ToString());
                if (sessionRole != 1)
                {
                    return RedirectToAction("Index", "Home");
                }
                return View("PackagesAdmin");
            }
            catch
            {
                return RedirectToAction("Index", "Home");
               
            }
        }

        public ActionResult ViewTracking()
        {
            try
            {
                int sessionRole = int.Parse(Session["role"].ToString());
                if (sessionRole != 1)
                {
                    return RedirectToAction("Index", "Home");
                }
                return View("TrackingAdmin");
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }

    }
}

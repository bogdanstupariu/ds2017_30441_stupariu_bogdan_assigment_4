﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GUI.UserService;

namespace GUI.Controllers
{
    public class HomeController : Controller
    {

        private UserServiceSoapClient _userServiceSOAP = new UserServiceSoapClient();

        public HomeController()
        {
            ViewBag.SuccessMsgRegister = "";
            ViewBag.ErrorMsgLogin = "";
            ViewBag.ErrorMsgRegister = "";
        }

        public ActionResult Index()
        {
            Session.RemoveAll();
            return View();
        }

        [HttpPost]
        public ActionResult Login(string user, string pass)
        {
            if(user == "" || pass == "")
            {
                ViewBag.ErrorMsgLogin = "Login failed";
                return View("Index");
            }

            User userResponse = _userServiceSOAP.Login(user,pass);
            if(userResponse.name == null )
            {
                ViewBag.ErrorMsgLogin = "Login failed. Wrong username and password";
                return View("Index");
            }

            bool role =(bool)userResponse.role;
            Session.Add("username", user);
            string usernameLogged = Session["username"].ToString();
            if (role)
            {
                Session.Add("role", 1);
                return RedirectToAction("ViewPackages", "Admin");
            } else
            {
                Session.Add("role", 0);
                return RedirectToAction("ViewUser", "User");
            }
        }

        [HttpPost]
        public ActionResult Register(string reguser, string regpass, string regpass2)
        {
            if (reguser == "" || regpass == "" || regpass2 == "")
            {
                ViewBag.ErrorMsgRegister = "Fields cannot be empty";
                return View("Index");
            }

            if ( regpass != regpass2)
            {
                ViewBag.ErrorMsgRegister = "Passwords should be identical";
                return View("Index");
            }

            if (_userServiceSOAP.CheckDuplicate(reguser))
            {
                ViewBag.ErrorMsgRegister = "Username already exists";
                return View("Index");
            }

            User newUser = new User();
            newUser.name = reguser;
            newUser.password = regpass;
            newUser.role = false;
            _userServiceSOAP.RegisterUser(newUser);

            ViewBag.ErrorMsgRegister = "";
            ViewBag.SuccessMsgRegister = "Account created! You can log in now";
            return View("Index");
            
            
        }
    }
}
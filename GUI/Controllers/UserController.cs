﻿using GUI.UserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace GUI.Controllers
{
    public class UserController : Controller
    {
        private UserServiceSoapClient _userServiceSOAP = new UserServiceSoapClient();
        public UserService.Package[] allMyPackages;
        public UserService.Tracking[] packageTrack;
        public UserService.Package[] allMyFilteredPackages;

        public UserController()
        {
            ViewBag.packageMyListFull = "";
            ViewBag.ErrorMsgSearch = "";
            ViewBag.filteredList = "";
            ViewBag.trackingList = "";


        }

        [HttpPost]
        public ActionResult Search(string id, string name)
        {
            if(id == "" && name == "")
            {
                ViewBag.ErrorMsgSearch = "Please fill in one field";
                return View("User");
            }

            Boolean found = false;

            allMyPackages = getAllMy();
            List<Package> allMyFilteredPackages = new List<Package>();

            if (id != "")
            {
                foreach(Package pack in allMyPackages)
                {
                    if(pack.id == int.Parse(id))
                    {
                        allMyFilteredPackages.Add(pack);
                        packageTrack = _userServiceSOAP.GetPackageTrack(int.Parse(pack.id.ToString()));
                        found = true;
                        break;
                    }
                }
            } else
            {
                foreach (Package pack in allMyPackages)
                {
                    if (pack.name == name)
                    {
                        allMyFilteredPackages.Add(pack);
                        packageTrack = _userServiceSOAP.GetPackageTrack(int.Parse(pack.id.ToString()));
                        found = true;
                        break;
                    }
                }
            }
            
            if(!found)
                ViewBag.ErrorMsgSearch = "No result found";

           

            ViewBag.trackingList = packageTrack;
            ViewBag.filteredList = allMyFilteredPackages;
            return View("User");
        }

        public ActionResult ViewUser()
        {
            try
            {
                int sessionRole = int.Parse(Session["role"].ToString());
                if (sessionRole != 0)
                {
                    return RedirectToAction("Index", "Home");
                }

                ViewBag.packageMyListFull = getAllMy();
                return View("User");
            }
            catch
            {
                return RedirectToAction("Index", "Home");

            }

        }

        private Package[] getAllMy()
        {
            string loggedUsername = Session["username"].ToString();
            allMyPackages = _userServiceSOAP.GetAllPackagesOfUser(loggedUsername);
            return allMyPackages;
        }
    }
}

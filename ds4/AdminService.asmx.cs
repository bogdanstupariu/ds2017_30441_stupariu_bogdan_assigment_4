﻿using DataRepository;
using DataRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ds4
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class AdminService : System.Web.Services.WebService
    {

        private UserRepository _userRepository;
        private PackageRepository _packageRepository;
        private TrackingRepository _trackingRepository;

        public AdminService()
        {
            _userRepository = new UserRepository();
            _packageRepository = new PackageRepository();
            _trackingRepository = new TrackingRepository();
        }

        [WebMethod]
        public void AddPackage(Package pack)
        {
            _packageRepository.Add(pack);
        }

        [WebMethod]
        public List<Package> GetAllPackages()
        {
            List<Package> allpck =  _packageRepository.GetAll();
            foreach(Package pck in allpck)
            {
                User user = _userRepository.GetById(pck.sender_id);
                pck.User = user;
                user = _userRepository.GetById(pck.receiver_id);
                pck.User1 = user;
            }
            return allpck;
        }

        [WebMethod]
        public void RemovePackage(int id)
        {
            Package pck = _packageRepository.GetById(id);
            _packageRepository.Delete(pck);
        }

        [WebMethod]
        public void TrackPackage(int id)
        {
            Package pck = _packageRepository.GetById(id);
            pck.tracking = true;
            _packageRepository.Update(pck);
            Tracking trk = new Tracking();
            trk.package_id = pck.id;
            trk.city = pck.sender_city;
            trk.date = DateTime.Now;
            _trackingRepository.Add(trk);
        }

        [WebMethod]
        public List<Tracking> GetPackageTrack(int id)
        {
            List<Tracking> allTr = _trackingRepository.GetAll();
            List<Tracking> myTr = new List<Tracking>();
            foreach (Tracking tr in allTr)
            {
                if(tr.package_id == id)
                {
                    Package pk = _packageRepository.GetById(tr.package_id);
                    tr.Package = pk;
                    myTr.Add(tr);
                }
            }
            return myTr;
        }

        [WebMethod]
        public void AddTrack(string city, int pack_id)
        {
            Tracking track = new Tracking();
            track.city = city;
            track.date = DateTime.Now;
            track.package_id = pack_id;
            _trackingRepository.Add(track);
        }
    }
}

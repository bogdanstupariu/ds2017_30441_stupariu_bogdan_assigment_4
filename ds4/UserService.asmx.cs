﻿using DataRepository;
using DataRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ds4
{

    [WebService(Namespace = "http://simpleprogrammer.com")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class UserService : WebService
    {

        private UserRepository _userRepository;
        private PackageRepository _packageRepository;
        private TrackingRepository _trackingRepository;

        public UserService()
        {
            _userRepository = new UserRepository();
            _packageRepository = new PackageRepository();
            _trackingRepository = new TrackingRepository();
        }

        [WebMethod]
        public User Login(string user, string pass)
        {
            List<User> users = _userRepository.GetAll();
            User userFound = new User();
            foreach(User u in users){
                if(u.name == user && pass == u.password)
                {
                    userFound = u;
                }
            }
            return userFound;
        }

        [WebMethod]
        public List<User> GetAllUsers()
        {
            return _userRepository.GetAll();
        }

        [WebMethod]
        public Boolean CheckDuplicate(string userName)
        {
            List<User> users = _userRepository.GetAll();
            User userFound = new User();
            foreach (User u in users)
            {
                if(u.name == userName)
                {
                    return true;
                }
            }
            return false;
        }

        [WebMethod]
        public void RegisterUser(User user)
        {
            _userRepository.Add(user);
        }

        [WebMethod]
        public List<Tracking> GetPackageTrack(int id)
        {
            List<Tracking> allTr = _trackingRepository.GetAll();
            List<Tracking> myTr = new List<Tracking>();
            foreach (Tracking tr in allTr)
            {
                if (tr.package_id == id)
                {
                    Package pk = _packageRepository.GetById(tr.package_id);
                    tr.Package = pk;
                    myTr.Add(tr);
                }
            }
            return myTr;
        }

        [WebMethod]
        public List<Package> GetAllPackagesOfUser(string username)
        {
            List<Package> allPackages = _packageRepository.GetAll();
            List<Package> userPackages = new List<Package>();
            foreach(Package pack in allPackages)
            {
                bool found = false;
                int userId = pack.sender_id;
                try
                {
                    User user = _userRepository.GetById(userId);
                    if(user != null)
                    {
                        if(user.name == username)
                        {
                            User us = _userRepository.GetById(pack.sender_id);
                            pack.User = us;
                            us = _userRepository.GetById(pack.receiver_id);
                            pack.User1 = us;
                            userPackages.Add(pack);
                            found = true;
                        }
                    }
                }
                catch
                {
                    //nothing
                }

                if (!found)
                {
                    userId = pack.receiver_id;
                    try
                    {
                        User user = _userRepository.GetById(userId);
                        if (user != null)
                        {
                            if (user.name == username)
                            {
                                User us = _userRepository.GetById(pack.sender_id);
                                pack.User = us;
                                us = _userRepository.GetById(pack.receiver_id);
                                pack.User1 = us;
                                userPackages.Add(pack);
                                found = true;
                            }
                        }
                    }
                    catch
                    {
                        //nothing
                    }
                }
            }

            return userPackages;

        }
     
    }
}

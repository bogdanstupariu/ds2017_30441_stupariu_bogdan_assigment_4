﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataRepository.Repository
{
    public class TrackingRepository
    {
        public void Add(Tracking trk)
        {
            using (var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.Trackings.Add(trk);
                context.SaveChanges();
            }
        }

        public List<Tracking> GetAll()
        {
            using (var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var trk = context.Trackings;
                return trk.ToList();
            }
        }

      
    }
}

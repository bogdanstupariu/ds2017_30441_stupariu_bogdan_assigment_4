﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataRepository.Repository
{
    public class PackageRepository
    {
        public void Add(Package pck)
        {
            using (var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.Packages.Add(pck);
                context.SaveChanges();
            }
        }

        public List<Package> GetAll()
        {
            using (var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var packages = context.Packages;
                if(packages != null) { 
                    return packages.ToList();
                } return null;
            }
        }

        public Package GetById(int id)
        {
            using (var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var packageF = context.Packages.FirstOrDefault(package=> package.id== id);
                return packageF;
            }
        }

        public void Update(Package pck)
        {
            using(var context= new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var pckUpdated = context.Packages.FirstOrDefault(package => package.id == pck.id);
                if(pckUpdated != null)
                {
                    pckUpdated.tracking = pck.tracking;
                    context.SaveChanges();
                }
            }
        }

        public void Delete(Package pck)
        {
            using(var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var pckDelete = context.Packages.FirstOrDefault(package => package.id == pck.id);
                if(pckDelete != null)
                {
                    context.Packages.Remove(pckDelete);
                    context.SaveChanges();
                }
            }
        }
    }
}

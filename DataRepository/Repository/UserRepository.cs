﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataRepository.Repository
{
    public class UserRepository
    {
        public void Add(User user)
        {
            using(var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public List<User> GetAll()
        {
            using (var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var users = context.Users;
                return users.ToList();
            }
        }

        public User GetByUsername(string name)
        {
            using (var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var userF = context.Users.FirstOrDefault(user => user.name == name);
                return userF;
            }
        }

        public User GetById(int id)
        {
            using (var context = new TrackingEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var userF = context.Users.FirstOrDefault(user => user.id== id);
                return userF;
            }
        }
    }
}
